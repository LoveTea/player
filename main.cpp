#include <QtGui/QGuiApplication>
#include <QtQml/QQmlApplicationEngine>

#include "src/player.h"

int main(int argc, char** argv) {
    QGuiApplication application(argc, argv);

    qmlRegisterType<player::Player>("com.player.player", 1, 0, "Player");

    QQmlApplicationEngine engine;
    engine.load(QUrl("qrc:/main.qml"));

    return application.exec();
}