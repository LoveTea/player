pragma Singleton

import QtQuick 2.12

QtObject {
    property QtObject basic: QtObject {
        property int margin: 5
        property int spacing: 0
    }

    property QtObject header: QtObject {
        property int height: 40
    }

    property QtObject footer: QtObject {
        property int height: 80
    }

    property QtObject button: QtObject {
        property int width: 40
        property int height: 40
    }

    property QtObject error: QtObject {
        property string textColor: "#a94442"
        property string borderColor: "#ebccd1"
        property string backgroundColor: "#f2dede"
    }

    property QtObject text: QtObject {
        property int h1: 18
        property int h2: 16
        property int padding: 6
        property int regular: 14
    }
}