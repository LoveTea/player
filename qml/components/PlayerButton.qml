import QtQuick 2.12
import QtQuick.Controls 2.12

RoundButton {
    radius: 10
    icon.width: 24
    icon.height: 24
}