import QtQuick 2.12
import QtMultimedia 5.8
import QtQuick.Layouts 1.12
import QtQuick.Controls 1.4
import QtQuick.Controls 2.12

import "."
import "components"
import com.player.player 1.0

ApplicationWindow {
    id: mainWindow
    minimumWidth: 600
    minimumHeight: 480
    visible: true

    ColumnLayout {
        id: mainLayout
        spacing: Style.basic.spacing
        anchors.fill: parent

        SplitView {
            id: bodyLayout
            Layout.fillWidth: true
            Layout.fillHeight: true
            orientation: Qt.Horizontal
            handleDelegate: Rectangle {}

            ColumnLayout {
                id: playlistLayout
                width: 200
                Layout.fillHeight: true

                Text {
                    id: playlistLabel
                    text: qsTr("Playlist")
                    font.pixelSize: Style.text.h2
                    padding: Style.text.padding
                    verticalAlignment: Text.AlignBottom
                    Layout.minimumHeight: Style.header.height
                    Layout.fillWidth: true
                    elide: Text.ElideRight
                }

                ListView {
                    id: playlist
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    spacing: Style.basic.spacing
                    model: ListModel {}
                    highlight: Rectangle {
                        color: 'lightgrey'
                    }
                    delegate: Text {
                        text: url
                        width: parent.width
                        elide: Text.ElideRight
                        padding: Style.text.padding

                         MouseArea {
                             anchors.fill: parent
                             onClicked: playlist.currentIndex = index
                             onDoubleClicked: player.startPlayback(parent.text)
                         }
                    }
                }
            }

            ColumnLayout {
                id: videoLayout
                Layout.fillWidth: true
                Layout.fillHeight: true

                RowLayout {
                    id: headerLayout
                    spacing: 5
                    Layout.topMargin: Style.basic.margin
                    Layout.rightMargin: Style.basic.margin
                    Layout.fillWidth: true
                    height: Style.header.height
                    Layout.alignment: Qt.AlignTop

                    TextField {
                        id: searchTextField
                        placeholderText: "RTSP stream address"
                        font.pixelSize: Style.text.h2
                        selectByMouse: true
                        Layout.fillWidth: true
                        Layout.minimumHeight: 38
                        Layout.maximumHeight: 38
                        onAccepted: searchButton.clicked()

                        ToolTip {
                            id: searchToolTip
                            contentItem: Text {
                                text: searchToolTip.text
                                font.pixelSize: Style.text.regular
                                color: Style.error.textColor
                                padding: Style.text.padding
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignBottom
                            }
                            background: Rectangle {
                                color: Style.error.backgroundColor
                                border.color: Style.error.borderColor
                            }
                        }
                    }

                    PlayerButton {
                        id: searchButton
                        icon.source: "qrc:///images/search.png"
                        Layout.alignment: Qt.AlignRight
                        Layout.minimumWidth: Style.button.width
                        Layout.maximumWidth: Style.button.width
                        Layout.minimumHeight: Style.button.height
                        Layout.maximumHeight: Style.button.height
                        onClicked: function() {
                            var pattern = /rtsp:\/\/[-a-zA-Z0-9@:%._+~#=]{2,256}[-a-zA-Z0-9@:%._\/?&+~#=]{0,255}/;
                            if (!pattern.test(searchTextField.text)) {
                                searchToolTip.show("Not valid RTSP address!", 1500);
                            } else {
                                var isFound = false;
                                var url = searchTextField.text.toLowerCase();
                                for (var i = 0; i < playlist.model.count; i++) {
                                    if (playlist.model.get(i).url == url) {
                                        isFound = true;
                                        playlist.currentIndex = i;
                                        break;
                                    }
                                }

                                if (!isFound) {
                                    playlist.model.append({ url: url });
                                    playlist.currentIndex = playlist.model.count - 1;
                                }

                                player.startPlayback(searchTextField.text);
                            }
                        }
                    }
                }

                Rectangle {
                    color: "black"
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    VideoOutput {
                        id: screen
                        source: player
                        anchors.fill: parent
                        fillMode: VideoOutput.PreserveAspectFit
                    }
                }
            }
        }

        ColumnLayout {
            id: footerLayout
            spacing: Style.basic.spacing
            Layout.fillWidth: true
            Layout.minimumHeight: Style.footer.height
            Layout.maximumHeight: Style.footer.height

            RowLayout {
                id: progressLayout
                spacing: Style.basic.spacing
                Layout.fillWidth: true
                Layout.minimumHeight: 30
                Layout.maximumHeight: 30

                Label {
                    id: elapsedTimeLabel
                    text: qsTr("--:--")
                    font.pixelSize: Style.text.regular
                    padding: Style.text.padding
                    Layout.fillHeight: true
                    Layout.minimumWidth: 20
                    Layout.alignment: Qt.AlignLeft
                }

                ProgressBar {
                    id: elapsedTimeProgressBar
                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    MouseArea {
                        anchors.fill: parent
                        onClicked: function(event) {
                            parent.value = event.x / parent.width;
                            player.seek(parent.value);
                        }
                    }
                }

                Label {
                    id: overallTimeLabel
                    text: qsTr("--:--")
                    font.pixelSize: Style.text.regular
                    padding: Style.text.padding
                    Layout.fillHeight: true
                    Layout.minimumWidth: 20
                    Layout.alignment: Qt.AlignLeft
                }
            }

            RowLayout {
                id: controlLayout
                spacing: 5
                Layout.leftMargin: Style.basic.margin
                Layout.bottomMargin: Style.basic.margin
                Layout.fillWidth: true
                Layout.minimumHeight: 45
                Layout.maximumHeight: 45

                PlayerButton {
                    id: playPauseButton
                    icon.source: "qrc:///images/play.png"
                    Layout.minimumWidth: Style.button.width
                    Layout.maximumWidth: Style.button.width
                    Layout.minimumHeight: Style.button.height
                    Layout.maximumHeight: Style.button.height
                    onClicked: function() {
                        if (player.state != Player.Play) {
                            var selectedRow = playlist.model.get(playlist.currentIndex);
                            if (selectedRow) {
                                player.startPlayback(selectedRow.url);
                            }
                        } else {
                            player.pausePressed();
                        }
                    }
                }

                PlayerButton {
                    id: previousButton
                    icon.source: "qrc:///images/previous.png"
                    Layout.minimumWidth: Style.button.width
                    Layout.maximumWidth: Style.button.width
                    Layout.minimumHeight: Style.button.height
                    Layout.maximumHeight: Style.button.height
                    onClicked: function() {
                        var index = undefined;
                        if (playlist.model.count == 0) {
                            return;
                        } else if (playlist.model.count == 1) {
                            index = playlist.currentIndex;
                        } else {
                            if (playlist.currentIndex == 0) {
                                index = playlist.model.count - 1;
                            } else {
                                index = playlist.currentIndex - 1;
                            }
                        }

                        if (index !== undefined) {
                            var row = playlist.model.get(index);
                            if (row) {
                                playlist.currentIndex = index;
                                player.startPlayback(row.url);
                            }
                        }
                    }
                }

                PlayerButton {
                    id: stopButton
                    icon.source: "qrc:///images/stop.png"
                    Layout.minimumWidth: Style.button.width
                    Layout.maximumWidth: Style.button.width
                    Layout.minimumHeight: Style.button.height
                    Layout.maximumHeight: Style.button.height
                    onClicked: player.stopPressed()
                }

                PlayerButton {
                    id: nextButton
                    icon.source: "qrc:///images/next.png"
                    Layout.minimumWidth: Style.button.width
                    Layout.maximumWidth: Style.button.width
                    Layout.minimumHeight: Style.button.height
                    Layout.maximumHeight: Style.button.height
                    onClicked: function() {
                        var index = undefined;
                        if (playlist.model.count == 0) {
                            return;
                        } else if (playlist.model.count == 1) {
                            index = playlist.currentIndex;
                        } else {
                            if (playlist.model.count - 1 == playlist.currentIndex) {
                                index = 0;
                            } else {
                                index = playlist.currentIndex + 1;
                            }
                        }

                        if (index !== undefined) {
                            var row = playlist.model.get(index);
                            if (row) {
                                playlist.currentIndex = index;
                                player.startPlayback(row.url);
                            }
                        }
                    }
                }

                Rectangle {
                    id: emptySpaceRectangle
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                }

                PlayerButton {
                    id: muteButton
                    icon.source: "qrc:///images/mute.png"
                    Layout.minimumWidth: Style.button.width
                    Layout.maximumWidth: Style.button.width
                    Layout.minimumHeight: Style.button.height
                    Layout.maximumHeight: Style.button.height
                    onClicked: function() {
                        if (player.muted) {
                            player.muted = false;
                            soundSlider.value = player.volume;
                            this.icon.source = "qrc:///images/mute.png";
                        } else {
                            player.muted = true;
                            soundSlider.value = 0;
                            this.icon.source = "qrc:///images/unmute.png";
                        }
                    }
                }

                Slider {
                    id: soundSlider
                    value: 1
                    stepSize: 0.01
                    Layout.fillHeight: true
                    onValueChanged: function() {
                        if (player.muted) {
                            if (soundSlider.value > 0) {
                                player.muted = false;
                                player.volume = soundSlider.value;
                                muteButton.icon.source = "qrc:///images/mute.png";
                            }
                        } else {
                            player.volume = soundSlider.value;
                        }
                    }
                }
            }
        }
    }

    Popup {
        id: errorPopup
        parent: Overlay.overlay
        width: parent.width
        height: parent.height
        background: Rectangle {
            color: "#80444444"
        }

        Rectangle {
            id: errorItem
            width: 250
            height: 150
            anchors.centerIn: parent
            color: Style.error.backgroundColor
            border.color: Style.error.borderColor

            ColumnLayout {
                id: errorLayout
                anchors.fill: parent
                spacing: Style.basic.spacing

                Label {
                    id: errorTitleLabel
                    text: qsTr("Error!")
                    font.pixelSize: Style.text.h2
                    color: Style.error.textColor
                    padding: Style.text.padding
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignBottom
                    Layout.fillWidth: true
                    Layout.minimumHeight: 20
                    Layout.alignment: Qt.AlignCenter
                }

                Label {
                    id: errorDescriptionLabel
                    font.pixelSize: Style.text.regular
                    color: Style.error.textColor
                    padding: Style.text.padding
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    Layout.fillWidth: true
                    Layout.minimumHeight: 60
                    Layout.alignment: Qt.AlignCenter
                }

                PlayerButton {
                    id: errorButton
                    text: qsTr("OK")
                    Layout.minimumHeight: Style.button.height
                    Layout.maximumHeight: Style.button.height
                    Layout.alignment: Qt.AlignCenter
                    onClicked: errorPopup.close()
                }
            }
        }
    }

    Player {
        id: player
        onStateChanged: function() {
            if (this.state == Player.Play) {
                playPauseButton.icon.source = "qrc:///images/pause.png";
            } else if (this.state == Player.Buffering) {
                elapsedTimeProgressBar.indeterminate = true
            } else {
                playPauseButton.icon.source = "qrc:///images/play.png";
            }

            if (this.state != Player.Buffering) {
                elapsedTimeProgressBar.indeterminate = false

                if (this.state != Player.Play && this.state != Player.Pause) {
                    elapsedTimeLabel.text = "--:--";
                    overallTimeLabel.text = "--:--";
                    elapsedTimeProgressBar.value = 0;
                }
            }
        }
        onTimeMetrics: function(currentSec, lengthSec) {
            var minutes = parseInt(currentSec / 60);
            var seconds = parseInt(currentSec % 60);
            elapsedTimeLabel.text = (minutes > 9 ? "" : "0") + minutes + ":" + (seconds > 9 ? "" : "0") + seconds;

            if (overallTimeLabel.text == "--:--") {
                minutes = parseInt(lengthSec / 60);
                seconds = parseInt(lengthSec % 60);
                overallTimeLabel.text = (minutes > 9 ? "" : "0") + minutes + ":" + (seconds > 9 ? "" : "0") + seconds;
            }

            elapsedTimeProgressBar.value = currentSec / lengthSec;
        }
        onPlayerError: function error(what) {
            errorDescriptionLabel.text = what;
            errorPopup.open();
            errorPopup.forceActiveFocus(Qt.OtherFocusReason);
        }
    }
}
