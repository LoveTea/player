#include <QtCore/QUrl>
#include <QtCore/QDebug>
#include <QtCore/QDateTime>
#include <QtCore/QCoreApplication>

#include "player.h"
#include "codec/audio/mp3.h"
#include "codec/video/h264.h"

namespace player {
    Player::Player(QObject* parent) : QObject(parent), state_(State::Stop), muted_(false), videoSurface_(nullptr),
        volume_(1), rtsp_(nullptr), audioCodec_(nullptr), videoCodec_(nullptr), audioOutput_(nullptr),
        audioClockRate_(0), videoClockRate_(0), playbackTimeSec_(0), playbackLengthSec_(0)
    {
        rtspThread_.setObjectName("RTSP");
        audioDecoderThread_.setObjectName("Audio decoder");
        videoDecoderThread_.setObjectName("Video decoder");

        connect(this, &Player::startPlayback, &Player::onStartPlayback);
        connect(this, &Player::pausePressed, &Player::onPausePressed);
        connect(this, &Player::stopPressed, &Player::onStopPressed);
        connect(this, &Player::mutedChanged, &Player::onMutedChanged);
        connect(this, &Player::volumeChanged, &Player::onVolumeChanged);
        connect(this, &Player::outputError, &Player::onPlayerError);
        connect(this, &Player::seek, &Player::onSeek);
        connect(&audioOutputTimer_, &QTimer::timeout, this, &Player::onAudioOutputTimer);
        connect(&videoOutputTimer_, &QTimer::timeout, this, &Player::onVideoOutputTimer);
        connect(&playbackProgressTimer_, &QTimer::timeout, this, &Player::onPlaybackProgressTimer);
    }

    Player::~Player() {
        stopPlayback();

        if (audioOutput_) {
            delete audioOutput_;
        }

        rtspThread_.quit();
        rtspThread_.wait();

        audioDecoderThread_.quit();
        audioDecoderThread_.wait();

        videoDecoderThread_.quit();
        videoDecoderThread_.wait();
    }

    void Player::RTSPConnect(const QUrl& url) {
        rtsp_ = new rtsp::RTSP(url);
        rtsp_->moveToThread(&rtspThread_);
        connect(&rtspThread_, &QThread::finished, rtsp_, &rtsp::RTSP::deleteLater, Qt::QueuedConnection);

        connect(rtsp_, &rtsp::RTSP::rtspError, this, &Player::onRtspError, Qt::QueuedConnection);
        connect(rtsp_, &rtsp::RTSP::rtspPlay, this, &Player::onRtspPlay, Qt::QueuedConnection);
        connect(rtsp_, &rtsp::RTSP::codecSettings, this, &Player::onCodecSettings, Qt::QueuedConnection);
        connect(rtsp_, &rtsp::RTSP::encodedFrame, this, &Player::onEncodedFrame, Qt::QueuedConnection);

        rtspThread_.start();

        QMetaObject::invokeMethod(rtsp_, "onStartConnection", Qt::QueuedConnection);
    }

    void Player::pausePlayback() {
        audioOutputTimer_.stop();
        videoOutputTimer_.stop();
        playbackProgressTimer_.stop();

        decodedAudioBuffer_.clear();
        decodedVideoBuffer_.clear();

        QCoreApplication::removePostedEvents(this, 0);
    }

    void Player::stopPlayback() {
        if (videoSurface_) {
            videoSurface_->present(QVideoFrame());
        }

        if (rtsp_) {
            rtsp_->disconnect(this);
            rtsp_->deleteLater();
            rtsp_ = nullptr;
        }

        if (audioCodec_) {
            audioCodec_->deleteLater();
            audioCodec_ = nullptr;
        }

        if (videoCodec_) {
            videoCodec_->deleteLater();
            videoCodec_ = nullptr;
        }

        pausePlayback();
    }

    int Player::audioOutputStart() {
        if (audioOutput_) {
            audioOutput_->deleteLater();
        }

        QAudioFormat format;
        format.setSampleSize(16);
        format.setChannelCount(2);
        format.setSampleRate(44100);
        format.setCodec("audio/pcm");
        format.setSampleType(QAudioFormat::SignedInt);
        format.setByteOrder(QAudioFormat::LittleEndian);

        audioOutput_ = new QAudioOutput(format);
        audioOutput_->setVolume(muted_ ? 0 : volume_);
        audioDevice_ = audioOutput_->start();
        if (!audioDevice_) {
            qCritical() << "Audio output acquire failed!";

            return -1;
        }

        return 0;
    }

    int Player::videoSurfaceStart(const QVideoFrame& frame) {
        if (videoSurface_ && !videoSurface_->isActive()) {
            QVideoSurfaceFormat surfaceFormat({ frame.width(), frame.height() }, frame.pixelFormat());
            if (!videoSurface_->start(surfaceFormat)) {
                qCritical() << "Video surface start failed!";

                return -1;
            }
        }

        return 0;
    }

    void Player::initMP3Codec() {
        audioCodec_ = new codec::audio::MP3;
        audioCodec_->moveToThread(&audioDecoderThread_);
        connect(&audioDecoderThread_, &QThread::finished, audioCodec_, &codec::audio::MP3::deleteLater, Qt::QueuedConnection);

        connect(audioCodec_, &codec::audio::MP3::decodedFrame, this, &Player::onDecodedFrame, Qt::QueuedConnection);
        connect(audioCodec_, &codec::audio::MP3::codecError, this, &Player::onCodecError, Qt::QueuedConnection);
        connect(this, &Player::mpaEncodedFrame, (codec::audio::MP3*) audioCodec_, &codec::audio::MP3::onEncodedFrame,
            Qt::QueuedConnection);

        audioDecoderThread_.start();
    }

    void Player::initH264Codec() {
        videoCodec_ = new codec::video::H264;
        videoCodec_->moveToThread(&videoDecoderThread_);
        connect(&videoDecoderThread_, &QThread::finished, videoCodec_, &codec::video::H264::deleteLater, Qt::QueuedConnection);

        connect(videoCodec_, &codec::video::H264::decodedFrame, this, &Player::onDecodedFrame, Qt::QueuedConnection);
        connect(videoCodec_, &codec::video::H264::codecError, this, &Player::onCodecError, Qt::QueuedConnection);
        connect(this, &Player::h264Settings, (codec::video::H264*) videoCodec_, &codec::video::H264::onDecoderSettings,
            Qt::QueuedConnection);
        connect(this, &Player::h264EncodedFrame, (codec::video::H264*) videoCodec_, &codec::video::H264::onEncodedFrame,
            Qt::QueuedConnection);

        videoDecoderThread_.start();
    }

    bool Player::isAudioBufferReady() {
        if (decodedAudioBuffer_.size() < 2) {
            return false;
        }

        quint32 startEndDifference = audioClockRate_ / 1000 * BUFFER_SIZE_MS;

        return decodedAudioBuffer_.last().timestamp - decodedAudioBuffer_.first().timestamp >= startEndDifference;
    }

    bool Player::isVideoBufferReady() {
        if (decodedVideoBuffer_.size() < 2) {
            return false;
        }

        quint32 startEndDifference = videoClockRate_ / 1000 * BUFFER_SIZE_MS;

        return decodedVideoBuffer_.last().timestamp - decodedVideoBuffer_.first().timestamp >= startEndDifference;
    }

    void Player::setVideoSurface(QAbstractVideoSurface* surface) {
        if (videoSurface_ != surface && videoSurface_ && videoSurface_->isActive()) {
            videoSurface_->stop();
        }

        videoSurface_ = surface;
    }

    void Player::onStartPlayback(const QString& url) {
        if (state_ == State::Pause && rtsp_ && rtsp_->getUrl() == url) {
            QMetaObject::invokeMethod(rtsp_, "onResume", Qt::QueuedConnection);
        } else {
            stopPlayback();
            setProperty("state", QVariant::fromValue(State::Stop));
            RTSPConnect(QUrl(url));
        }

        setProperty("state", QVariant::fromValue(State::Buffering));
    }

    void Player::onPausePressed() {
        if (rtsp_) {
            pausePlayback();
            QMetaObject::invokeMethod(rtsp_, "onPause", Qt::QueuedConnection);
        }

        setProperty("state", QVariant::fromValue(State::Pause));
    }

    void Player::onStopPressed() {
        stopPlayback();

        setProperty("state", QVariant::fromValue(State::Stop));
    }

    void Player::onMutedChanged(bool isMuted) {
        if (audioOutput_) {
            audioOutput_->setVolume(isMuted ? 0 : volume_);
        }
    }

    void Player::onVolumeChanged(float volume) {
        if (audioOutput_) {
            audioOutput_->setVolume(volume);
        }
    }

    void Player::onSeek(float position) {
        if (playbackLengthSec_ > 0) {
            pausePlayback();
            setProperty("state", QVariant::fromValue(State::Pause));

            QMetaObject::invokeMethod(rtsp_, "onPause", Qt::QueuedConnection);
            QMetaObject::invokeMethod(rtsp_, "onResume", Qt::QueuedConnection,
                Q_ARG(float, playbackLengthSec_ * position));
        }
    }

    void Player::onPlayerError(QString what) {
        qCritical() << "Player error!" << what;

        stopPlayback();

        setProperty("state", QVariant::fromValue(State::Stop));

        emit playerError("Player inner error!");
    }

    void Player::onAudioOutputTimer() {
        if (state_ != State::Play) {
            return;
        }

        if (decodedAudioBuffer_.empty()) {
            setProperty("state", QVariant::fromValue(State::Buffering));

            return;
        }

        float clockRateToMs = (float) audioClockRate_ / 1000;
        if (clockRateToMs == 0) {
            emit outputError("Clock rate conversion failed!");

            return;
        }

        while (decodedAudioBuffer_.first().timestamp / clockRateToMs <= wallClock_.msecsTo(QDateTime::currentDateTime())) {
            if (!audioOutput_) {
                if (audioOutputStart() != 0) {
                    emit outputError("Audio output start failed!");

                    return;
                }
            } else if (audioOutput_->state() != QAudio::ActiveState && audioOutput_->state() != QAudio::IdleState) {
                audioDevice_ = audioOutput_->start();
                if (!audioDevice_) {
                    emit outputError("Audio device acquire failed!");

                    return;
                }
            }

            codec::DecodedAudioFrame frame = decodedAudioBuffer_.first();

            audioDevice_->write(frame.data);

            decodedAudioBuffer_.remove(decodedAudioBuffer_.firstKey());

            if (decodedAudioBuffer_.empty()) {
                return;
            }
        }
    }

    void Player::onVideoOutputTimer() {
        if (state_ != State::Play) {
            return;
        }

        if (decodedVideoBuffer_.empty()) {
            setProperty("state", QVariant::fromValue(State::Buffering));

            return;
        }

        float clockRateToMs = (float) videoClockRate_ / 1000;
        if (clockRateToMs == 0) {
            emit outputError("Clock rate conversion failed!");

            return;
        }

        while (decodedVideoBuffer_.first().timestamp / clockRateToMs <= wallClock_.msecsTo(QDateTime::currentDateTime())) {
            codec::DecodedVideoFrame frame = decodedVideoBuffer_.first();

            if (!videoSurface_) {
                emit outputError("Video surface not set");

                return;
            } else if (!videoSurface_->isActive()) {
                if (videoSurfaceStart(frame.data) != 0) {
                    emit outputError("Video surface not in active state!");

                    return;
                }
            }

            if (!videoSurface_->present(frame.data)) {
                emit outputError("Video frame present failed!");

                return;
            }

            decodedVideoBuffer_.remove(decodedVideoBuffer_.firstKey());

            if (decodedVideoBuffer_.empty()) {
                return;
            }
        }
    }

    void Player::onPlaybackProgressTimer() {
        if (state_ != State::Play && state_ != State::Buffering) {
            return;
        }

        playbackTimeSec_ = float(wallClock_.msecsTo(QDateTime::currentDateTime())) / 1000;
        emit timeMetrics(playbackTimeSec_, playbackLengthSec_);

        if (playbackTimeSec_ > playbackLengthSec_) {
            stopPlayback();

            setProperty("state", QVariant::fromValue(State::Stop));
        }
    }

    void Player::onRtspError(QString what) {
        qCritical() << "RTSP error!" << what;

        stopPlayback();

        setProperty("state", QVariant::fromValue(State::Stop));

        emit playerError("RTSP error!");
    }

    void Player::onRtspPlay(float currentSec, float lengthSec) {
        playbackTimeSec_ = currentSec;
        playbackLengthSec_ = lengthSec;

        emit timeMetrics(playbackTimeSec_, playbackLengthSec_);

        setProperty("state", QVariant::fromValue(State::Buffering));
    }

    void Player::onCodecSettings(rtsp::SDP::Media::Properties settings) {
        if (state_ != State::Buffering && state_ != State::Play) {
            return;
        }

        switch (settings.type) {
            case rtsp::RTP::PayloadType::MPA:
                audioClockRate_ = settings.clockRate;

                break;

            case rtsp::RTP::PayloadType::H264: {
                videoClockRate_ = settings.clockRate;

                if (!videoCodec_) {
                    initH264Codec();
                }

                codec::video::H264::Params params;
                params.version = settings.version;
                params.profile = settings.profile;
                params.compatibility = settings.compatibility;
                params.level = settings.level;
                params.sps = settings.sps;
                params.pps = settings.pps;
                params.width = settings.width;
                params.height = settings.height;
                emit h264Settings(QVariant::fromValue(params));

                break;
            }

            default:
                emit outputError("Not supported codec!");

                break;
        }
    }

    void Player::onEncodedFrame(rtsp::RTP::EncodedFrame frame) {
        if (state_ != State::Buffering && state_ != State::Play) {
            return;
        }

        switch (frame.type) {
            case rtsp::RTP::PayloadType::MPA: {
                if (!audioCodec_) {
                    initMP3Codec();
                }

                codec::EncodedAudioFrame audioFrame {
                    .timestamp = frame.timestamp,
                    .data = frame.data
                };
                emit mpaEncodedFrame(QVariant::fromValue(audioFrame));

                break;
            }

            case rtsp::RTP::PayloadType::H264: {
                if (!videoCodec_) {
                    initH264Codec();
                }

                codec::EncodedVideoFrame videoFrame {
                    .timestamp = frame.timestamp,
                    .data = frame.data
                };
                emit h264EncodedFrame(QVariant::fromValue(videoFrame));

                break;
            }

            default:
                emit playerError("Unsupported media type!");

                break;
        }
    }

    void Player::onCodecError(QString what) {
        qCritical() << "Codec error!" << what;

        stopPlayback();

        setProperty("state", QVariant::fromValue(State::Stop));

        emit playerError("Codec error!");
    }

    void Player::onDecodedFrame(QVariant frame) {
        if (state_ != State::Buffering && state_ != State::Play) {
            return;
        }

        if (frame.canConvert<codec::DecodedAudioFrame>()) {
            auto audioFrame = frame.value<codec::DecodedAudioFrame>();
            decodedAudioBuffer_[audioFrame.timestamp] = audioFrame;
        } else if (frame.canConvert<codec::DecodedVideoFrame>()) {
            auto videoFrame = frame.value<codec::DecodedVideoFrame>();
            decodedVideoBuffer_[videoFrame.timestamp] = videoFrame;
        } else {
            emit outputError("Unknown frame type!");

            return;
        }

        if (state_ == State::Buffering) {
            if (isAudioBufferReady() && isVideoBufferReady()) {
                setProperty("state", QVariant::fromValue(State::Play));
                wallClock_.setMSecsSinceEpoch(QDateTime::currentMSecsSinceEpoch() - quint64(playbackTimeSec_ * 1000));
                audioOutputTimer_.start(10);
                videoOutputTimer_.start(10);
                playbackProgressTimer_.start(500);
            }
        }
    }
}