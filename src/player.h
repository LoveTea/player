#ifndef PLAYER_PLAYER_H
#define PLAYER_PLAYER_H

#include <QtCore/QTimer>
#include <QtCore/QObject>
#include <QtCore/QThread>
#include <QtCore/QDateTime>
#include <QtMultimedia/QAudioOutput>
#include <QtMultimedia/QVideoSurfaceFormat>
#include <QtMultimedia/QAbstractVideoSurface>

#include "rtsp/rtp.h"
#include "rtsp/rtsp.h"
#include "codec/codec.h"
#include "codec/frame.h"


namespace player {
    class Player : public QObject {
        Q_OBJECT
        Q_DISABLE_COPY(Player)

    public:
        enum class State { Stop, Play, Pause, Buffering };
        Q_ENUM(State)

    private:
        static const int BUFFER_SIZE_MS = 1000;

        State state_;
        bool muted_;
        float volume_;

        QIODevice* audioDevice_;
        QAudioOutput* audioOutput_;
        QAbstractVideoSurface* videoSurface_;

        QTimer audioOutputTimer_;
        QTimer videoOutputTimer_;
        QTimer playbackProgressTimer_;

        QMap<quint32, codec::DecodedAudioFrame> decodedAudioBuffer_;
        QMap<quint32, codec::DecodedVideoFrame> decodedVideoBuffer_;

        QDateTime wallClock_;
        float playbackTimeSec_;
        float playbackLengthSec_;

        rtsp::RTSP* rtsp_;
        codec::Codec* audioCodec_;
        codec::Codec* videoCodec_;

        QThread rtspThread_;
        QThread audioDecoderThread_;
        QThread videoDecoderThread_;

        quint32 audioClockRate_;
        quint32 videoClockRate_;

        void RTSPConnect(const QUrl& url);
        void pausePlayback();
        void stopPlayback();

        int audioOutputStart();
        int videoSurfaceStart(const QVideoFrame& frame);

        void initMP3Codec();
        void initH264Codec();

        bool isAudioBufferReady();
        bool isVideoBufferReady();

    public:
        explicit Player(QObject* parent = nullptr);
        ~Player() override;

        Q_PROPERTY(State state MEMBER state_ READ state NOTIFY stateChanged)
        Q_PROPERTY(bool muted MEMBER muted_ NOTIFY mutedChanged)
        Q_PROPERTY(float volume MEMBER volume_ NOTIFY volumeChanged)
        Q_PROPERTY(QAbstractVideoSurface* videoSurface READ videoSurface WRITE setVideoSurface)

        inline QAbstractVideoSurface* videoSurface() const { return videoSurface_; }
        void setVideoSurface(QAbstractVideoSurface* surface);

        inline State state() const { return state_; }

    private slots:
        // player slots
        void onStartPlayback(const QString& url);
        void onPausePressed();
        void onStopPressed();
        void onMutedChanged(bool isMuted);
        void onVolumeChanged(float volume);
        void onSeek(float position);
        void onPlayerError(QString what);
        void onAudioOutputTimer();
        void onVideoOutputTimer();
        void onPlaybackProgressTimer();

        // RTSP slots
        void onRtspError(QString what);
        void onRtspPlay(float currentSec, float lengthSec);
        void onCodecSettings(rtsp::SDP::Media::Properties settings);
        void onEncodedFrame(rtsp::RTP::EncodedFrame frame);

        // codec slots
        void onCodecError(QString what);
        void onDecodedFrame(QVariant frame);

    signals:
        // player signals
        void startPlayback(QString url);
        void pausePressed();
        void stopPressed();
        void mutedChanged(bool isMuted);
        void volumeChanged(float volume);
        void timeMetrics(float currentSec, float lengthSec);
        void seek(float position);
        void stateChanged(State state);
        void outputError(QString what);
        void playerError(QString what);

        // codec signals
        void h264Settings(QVariant settings);
        void h264EncodedFrame(QVariant frame);
        void mpaEncodedFrame(QVariant frame);
    };
}


#endif //PLAYER_PLAYER_H
