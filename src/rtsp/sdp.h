#ifndef PLAYER_SDP_H
#define PLAYER_SDP_H

#include <QtCore/QHash>
#include <QtCore/QMetaType>
#include <QtCore/QByteArray>

#include "rtp.h"


namespace player::rtsp {
    class SDP {
    public:
        enum class Level { SESSION, MEDIA, NOT_SUPPORTED };

        class Media {
        public:
            enum class Type { AUDIO, VIDEO };

            struct Properties {
                RTP::PayloadType type;
                quint32 clockRate;
                quint8 version;
                quint8 profile;
                quint8 compatibility;
                quint8 level;
                QByteArray sps;
                QByteArray pps;
                int width;
                int height;
            };

        private:
            Type type_;
            quint16 port_;
            quint32 clockRate_;
            QByteArray proto_;
            quint16 formatId_;
            QByteArray format_;
            quint16 numberOfPorts_;
            quint16 channelsCount_;
            QHash<QByteArray, QByteArray> attributes_;

            int parseHeader(const QByteArray& data);
            int parseAttribute(const QByteArray& data);
            int parseRtpMapAttribute(const QByteArray& data);
            int parseMPAProps(const QByteArray& data, Properties& attributes) const;
            int parseH264Props(const QByteArray& data, Properties& attributes) const;

        public:
            inline Type getType() const { return type_; }
            inline quint16 getPort() const { return port_; }
            inline int getClockRate() const { return clockRate_; }
            inline quint16 getFormatId() const { return formatId_; }
            inline const QByteArray& getProto() const { return proto_; }
            inline const QByteArray getFormat() const { return format_; }
            inline quint16 getNumberOfPorts() const { return numberOfPorts_; }

            QByteArray getAttribute(const QByteArray& key) const;
            Properties getMPAAttributes() const;
            Properties getH264Attributes() const;

            friend class SDP;
        };

    private:
        int version_;
        Level level_;
        QList<Media> medias;
        QByteArray sessionName_;
        QByteArray sessionOwner_;
        QList<QByteArray> sessionAttributes_;

        int parseVersion(const QByteArray& data);
        int parseAttribute(const QByteArray& data);
        int parseSessionAttribute(const QByteArray& data);

    public:
        SDP();
        ~SDP() = default;

        int deserialize(const QByteArray& data);

        inline int getVersion() const { return version_; }
        inline const QList<Media>& getMedias() const { return medias; }
        inline const QByteArray& getSessionName() const { return sessionName_; }
        inline const QByteArray& getSessionOwner() const { return sessionOwner_; }
        inline const QList<QByteArray>& getSessionAttributes() const { return sessionAttributes_; }
    };
}

Q_DECLARE_METATYPE(player::rtsp::SDP::Media::Properties);


#endif //PLAYER_SDP_H
