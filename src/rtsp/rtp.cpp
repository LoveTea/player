#include <QtCore/QtEndian>

#include "rtp.h"

namespace player::rtsp {
    int RTP::Header::parseHeader(const QByteArray& data) {
        int processed = 0;

        version = (quint8) ((data[0] & 0b11000000) >> 6);
        padding = ((quint8) (data[0] & 0b00100000)) >> 5;
        extension = ((quint8) (data[0] & 0b00010000)) >> 4;
        csrcCount = (quint8) (data[0] & 0b00001111);
        marker = ((quint8) (data[1] & 0b10000000)) >> 7;
        payloadType = (quint8) (data[1] & 0b01111111);
        sequenceNumber = qFromBigEndian(*((quint16*) (data.data() + 2)));
        timestamp = qFromBigEndian(*(quint32*) (data.data() + 4));
        ssrc = qFromBigEndian(*(quint32*) (data.data() + 8));

        if (csrcCount > 0) {
            qCritical() << "RTP header parsing! Contributing sources not supported!";

            return -1;
        }

        processed = 12;

        return processed;
    }

    RTP::RTP(PayloadType type, QObject* parent) : QUdpSocket(parent), payloadType_(type) {
        header_ = {};

        connect(this, &RTP::readyRead, this, &RTP::onReadyRead);
    }

    int RTP::processMp3Nal(const QByteArray& data) {
        int processed = data.size();

        EncodedFrame frame;
        frame.data = data;
        frame.type = PayloadType::MPA;
        frame.timestamp = header_.timestamp;
        frame.sequenceNumber = header_.sequenceNumber;

        emit newFrame(frame);

        return processed;
    }

    int RTP::processH264Nal(const QByteArray& data) {
        int processed = data.size();

        int nalType = data[0] & 0x1f;
        if (nalType <= 23) {
            EncodedFrame frame;
            frame.data = data;
            frame.type = PayloadType::H264;
            frame.timestamp = header_.timestamp;
            frame.sequenceNumber = header_.sequenceNumber;

            emit newFrame(frame);
        } else if (nalType == 28) {
            if (data[1] & 0x80) {
                fragmentationUnit_.clear();
                fragmentationUnit_.push_back(quint8(data[0] & 0xe0) | quint8(data[1] & 0x1f));
                fragmentationUnit_.push_back(QByteArray(data.data() + 2, data.size() - 2));
            } else if (data[1] & 0x40) {
                fragmentationUnit_.push_back(QByteArray(data.data() + 2, data.size() - 2));

                EncodedFrame frame;
                frame.data = fragmentationUnit_;
                frame.type = PayloadType::H264;
                frame.timestamp = header_.timestamp;
                frame.sequenceNumber = header_.sequenceNumber;

                emit newFrame(frame);
            } else {
                fragmentationUnit_.push_back(QByteArray(data.data() + 2, data.size() - 2));
            }
        } else {
            qCritical() << "Not support H264 NAL type!" << nalType;

            return -1;
        }

        return processed;
    }

    void RTP::onReadyRead() {
        while (hasPendingDatagrams()) {
            QByteArray datagram((int) pendingDatagramSize(), 0);
            readDatagram(datagram.data(), datagram.size(), nullptr, nullptr);

#ifdef DEBUG
            qDebug() << "RTP PACKET" << datagram.size();
            for (int i = 0; i < datagram.size(); ++i) {
                if (i != 0 && i % 75 == 0) {
                    printf("\n");
                }
                printf("%02x ", quint8(datagram.data()[i]));
            }
#endif

            int processed = header_.parseHeader(datagram);
            if (processed == -1) {
                emit rtpError("RTP header parse error!");

                return;
            }

            QByteArray nal = QByteArray(datagram.data() + processed, datagram.size() - processed);

            switch (payloadType_) {
                case PayloadType::MPA:
                    processMp3Nal(nal);
                    break;

                case PayloadType::H264:
                    processH264Nal(nal);
                    break;

                default:
                    emit rtpError("RTP unknown payload type!");

                    return;
            }
        }
    }
}