#ifndef PLAYER_RTSP_H
#define PLAYER_RTSP_H

#include <QtCore/QUrl>
#include <QtNetwork/QTcpSocket>

#include "sdp.h"
#include "rtp.h"
#include "rtcp.h"
#include "message.h"


namespace player::rtsp {
    const QByteArray NEW_LINE = "\r\n";
    const QByteArray VERSION = "RTSP/1.0";

    class RTSP : public QTcpSocket {
        Q_OBJECT
        Q_DISABLE_COPY(RTSP)

    public:
        struct Transport {
            RTP* rtp;
            RTCP* rtcp;
            bool isSetup;
            QByteArray url;
            quint16 rtpPort;
            quint16 rtcpPort;
            quint16 rtpServerPort;
            quint16 rtcpServerPort;
        };

    private:
        QUrl url_;
        quint32 sequence_;
        QByteArray buffer_;
        QByteArray session_;
        float seekPositionSec_;
        QList<Transport> transports_;
        QList<QByteArray> allowedCommands_;
        QHash<quint32, int(RTSP::*)(const Message&)> sequenceToCallback_;

        void optionsRequest();
        void describeRequest();
        void setupRequest(const Transport& transport);
        void playRequest(float startTimeMs = 0);
        void pauseRequest();

        int optionsResponse(const Message& message);
        int describeResponse(const Message& message);
        int setupResponse(const Message& message);
        int playResponse(const Message& message);
        int pauseResponse(const Message& message);

        int openTransports(const QList<SDP::Media>& medias);
        int openTransport(const SDP::Media& media);

    public:
        explicit RTSP(const QUrl& url, QObject* parent = nullptr);
        ~RTSP() override = default;

        qint64 write(const QByteArray& data);

        inline const QUrl& getUrl() const { return url_; }

    private slots:
        void onReadyRead();
        void onRtspConnect();

    public slots:
        void onStartConnection();
        void onResume(float positionSec = 0);
        void onPause();
        void onSeek(float positionSec);

    signals:
        void rtspError(QString what);
        void rtspPlay(float currentSec, float lengthSec);
        void encodedFrame(RTP::EncodedFrame frame);
        void codecSettings(SDP::Media::Properties settings);
    };
}


#endif //PLAYER_RTSP_H
