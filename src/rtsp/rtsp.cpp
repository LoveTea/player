#include <QtNetwork/QHostAddress>
#include <QtCore/QRandomGenerator>

#include "sdp.h"
#include "rtsp.h"


namespace player::rtsp {
    RTSP::RTSP(const QUrl& url, QObject* parent) : QTcpSocket(parent), url_(url), sequence_(1), seekPositionSec_(-1) {
        connect(this, &RTSP::connected, this, &RTSP::onRtspConnect);
        connect(this, &RTSP::readyRead, this, &RTSP::onReadyRead);
        connect(this, &RTSP::disconnected, this, [this] { emit rtspError("Server disconnected!"); });
        connect(this, qOverload<QAbstractSocket::SocketError>(&rtsp::RTSP::error), this,
            [this](QAbstractSocket::SocketError error) { qCritical() << error; emit rtspError("Connection to server failed!"); });
    }

    void RTSP::optionsRequest() {
        sequenceToCallback_[sequence_] = &RTSP::optionsResponse;

        Message message;
        message.setCommand("OPTIONS");
        message.setUrl(url_);
        message.setVersion(VERSION);
        message.setHeader("CSeq", QByteArray::number(sequence_++));
        message.setHeader("User-Agent", "Super Player");

        write(message.serialize());
    }

    void RTSP::describeRequest() {
        if (!allowedCommands_.contains("DESCRIBE")) {
            emit rtspError("RTSP DESCRIBE command not allowed!");

            return;
        }

        sequenceToCallback_[sequence_] = &RTSP::describeResponse;

        Message message;
        message.setCommand("DESCRIBE");
        message.setUrl(url_);
        message.setVersion(VERSION);
        message.setHeader("CSeq", QByteArray::number(sequence_++));
        message.setHeader("User-Agent", "Super Player");
        message.setHeader("Accept", "application/sdp");

        write(message.serialize());
    }

    void RTSP::setupRequest(const Transport& transport) {
        if (!allowedCommands_.contains("SETUP")) {
            emit rtspError("RTSP SETUP command not allowed!");

            return;
        }

        sequenceToCallback_[sequence_] = &RTSP::setupResponse;

        QUrl streamUrl(transport.url);
        QByteArray transportHeader = QString("RTP/AVP;unicast;client_port=%1-%2")
            .arg(transport.rtpPort).arg(transport.rtcpPort).toUtf8();

        Message message;
        message.setCommand("SETUP");
        message.setUrl(streamUrl);
        message.setVersion(VERSION);
        message.setHeader("CSeq", QByteArray::number(sequence_++));
        message.setHeader("User-Agent", "Super Player");
        message.setHeader("Transport", transportHeader);
        if (!session_.isEmpty()) {
            message.setHeader("Session", session_);
        }

        write(message.serialize());
    }

    void RTSP::playRequest(float startTimeMs) {
        if (!allowedCommands_.contains("PLAY")) {
            emit rtspError("RTSP PLAY command not allowed!");

            return;
        }

        sequenceToCallback_[sequence_] = &RTSP::playResponse;

        Message message;
        message.setCommand("PLAY");
        message.setUrl(url_);
        message.setVersion(VERSION);
        message.setHeader("CSeq", QByteArray::number(sequence_++));
        message.setHeader("User-Agent", "Super Player");
        if (!session_.isEmpty()) {
            message.setHeader("Session", session_);
        }
        if (startTimeMs > 0) {
            message.setHeader("Range", QString("npt=%1-").arg(QString::number(startTimeMs, 'f', 3)).toUtf8());
        }

        write(message.serialize());
    }

    void RTSP::pauseRequest() {
        if (!allowedCommands_.contains("PAUSE")) {
            emit rtspError("RTSP PAUSE command not allowed!");

            return;
        }

        sequenceToCallback_[sequence_] = &RTSP::pauseResponse;

        Message message;
        message.setCommand("PAUSE");
        message.setUrl(url_);
        message.setVersion(VERSION);
        message.setHeader("CSeq", QByteArray::number(sequence_++));
        message.setHeader("User-Agent", "Super Player");
        if (!session_.isEmpty()) {
            message.setHeader("Session", session_);
        }

        write(message.serialize());
    }

    int RTSP::optionsResponse(const Message& message) {
        allowedCommands_ = message.parsePublicHeader();

        describeRequest();

        return 0;
    }

    int RTSP::describeResponse(const Message& message) {
        SDP sdp;
        if (int rc = sdp.deserialize(message.getBody()); rc < 0) {
            return rc;
        }

        session_ = message.getHeader("Session");

        if (int rc = openTransports(sdp.getMedias()); rc != 0) {
            return rc;
        }

        for (const auto& media : sdp.getMedias()) {
            if (media.getType() == SDP::Media::Type::AUDIO) {
                if (media.getFormat().toLower() == "mpa") {
                    emit codecSettings(media.getMPAAttributes());
                }
            } else if (media.getType() == SDP::Media::Type::VIDEO) {
                if (media.getFormat().toLower() == "h264") {
                    emit codecSettings(media.getH264Attributes());
                }
            }
        }

        if (!transports_.isEmpty()) {
            setupRequest(transports_.first());
        }

        return 0;
    }

    int RTSP::setupResponse(const Message& message) {
        QHash transportParameters = message.parseTransportHeader();
        auto iterator = transportParameters.find("client_port");
        if (iterator == transportParameters.end()) {
            qCritical() << "RTSP transport client port not found!";

            return -1;
        }

        for (auto& transport : transports_) {
            if (!iterator.value().startsWith(QByteArray::number(transport.rtpPort))) {
                continue;
            }

            iterator = transportParameters.find("server_port");
            if (iterator == transportParameters.end()) {
                qCritical() << "RTSP transport server port not found!";

                return -1;
            }

            QList ports = iterator.value().split('-');
            if (ports.size() != 2) {
                qCritical() << "RTSP transport server port parse error!" << ports;

                return -1;
            }

            bool isOk;
            transport.rtpServerPort = (quint16) ports[0].toInt(&isOk);
            if (!isOk) {
                qCritical() << "RTSP transport server port parse error!" << ports;

                return -1;
            }

            transport.rtcpServerPort = (quint16) ports[1].toInt(&isOk);
            if (!isOk) {
                qCritical() << "RTSP transport server port parse error!" << ports;

                return -1;
            }

            transport.isSetup = true;

            break;
        }

        for (const auto& transport : transports_) {
            if (!transport.isSetup) {
                setupRequest(transport);

                return 0;
            }
        }

        playRequest();

        return 0;
    }

    int RTSP::playResponse(const Message& message) {
        Message::RangeHeader range = message.parseRangeHeader();
        emit rtspPlay(range.currentSec, range.lengthSec);

        return 0;
    }

    int RTSP::pauseResponse(const Message& message) {
        if (seekPositionSec_ >= 0) {
            playRequest(seekPositionSec_);
            seekPositionSec_ = -1;
        }

        return 0;
    }

    int RTSP::openTransports(const QList<player::rtsp::SDP::Media> &medias) {
        for (const auto& media : medias) {
            if (transports_.size() == 2) {
                break;
            }

            if (media.getType() == SDP::Media::Type::AUDIO) {
                if (transports_.size() == 1 && transports_.back().rtp->getPayloadType() == RTP::PayloadType::MPA) {
                    continue;
                }

                if (media.getFormat().toLower() != "mpa") {
                    continue;
                }

                if (int rc = openTransport(media); rc != 0) {
                    return rc;
                }
            } else if (media.getType() == SDP::Media::Type::VIDEO) {
                if (transports_.size() == 1 && transports_.back().rtp->getPayloadType() == RTP::PayloadType::H264) {
                    continue;
                }

                if (media.getFormat().toLower() != "h264") {
                    continue;
                }

                if (int rc = openTransport(media); rc != 0) {
                    return rc;
                }
            }
        }

        if (transports_.empty()) {
            qCritical() << "RTSP open transport failed!";

            return -1;
        }

        return 0;
    }

    int RTSP::openTransport(const SDP::Media& media) {
        for (int i = 0; i < 3; ++i) {
            Transport transport;
            transport.isSetup = false;
            transport.url = media.getAttribute("control");
            if (!transport.url.toLower().trimmed().startsWith("rtsp://")) {
                transport.url = (url_.toString() + transport.url).toUtf8();
            }
            RTP::PayloadType type;
            QByteArray payloadFormat = media.getFormat().toLower();
            if (payloadFormat == "mpa") {
                type = RTP::PayloadType::MPA;
            } else if (payloadFormat == "h264") {
                type = RTP::PayloadType::H264;
            } else {
                qCritical() << "RTP payload type not supported!";

                return -1;
            }

            transport.rtpPort = (quint16) QRandomGenerator::global()->bounded(10000, 30000);
            if (transport.rtpPort % 2 == 1) {
                --transport.rtpPort;
            }

            transport.rtp = new RTP(type, this);
            if (!transport.rtp->bind(transport.rtpPort)) {
                qCritical() << "RTP bind error!" << transport.rtpPort;
                transport.rtp->deleteLater();

                continue;
            }

            connect(transport.rtp, &RTP::rtpError, this, &RTSP::rtspError);
            connect(transport.rtp, &RTP::newFrame, this, &RTSP::encodedFrame);

            transport.rtcp = new RTCP(this);
            transport.rtcpPort = (quint16) (transport.rtpPort + 1);
            if (!transport.rtcp->bind(transport.rtcpPort)) {
                qCritical() << "RTCP bind error!" << transport.rtcpPort;
                transport.rtcp->deleteLater();

                continue;
            }

            transports_.push_back(transport);

            return 0;
        }

        return -1;
    }

    qint64 RTSP::write(const QByteArray& data) {
        qDebug() << data.data();

        return QIODevice::write(data);
    }

    void RTSP::onRtspConnect() {
        optionsRequest();
    }

    void RTSP::onStartConnection() {
        connectToHost(url_.host(), quint16(url_.port() < 0 ? 554 : url_.port()));
    }

    void RTSP::onResume(float positionSec) {
        if (positionSec > 0) {
            playRequest(positionSec);
        } else {
            playRequest(-1);
        }
    }

    void RTSP::onPause() {
        pauseRequest();
    }

    void RTSP::onSeek(float positionSec) {
        seekPositionSec_ = positionSec;
        pauseRequest();
    }

    void RTSP::onReadyRead() {
        while (bytesAvailable()) {
            buffer_ += readAll();

            qDebug() << buffer_.data();

            while (true) {
                if (buffer_.isEmpty()) {
                    break;
                }

                Message message;
                int processed = message.deserialize(buffer_);
                if (processed == 0) {
                    return;
                } else if (processed == -1) {
                    emit rtspError("RTSP deserialize error!");

                    return;
                }

                buffer_ = QByteArray(buffer_.begin() + processed, buffer_.size() - processed);

                QByteArray sequenceString = message.getHeader("CSeq");
                if (sequenceString.isEmpty()) {
                    emit rtspError("RTSP CSeq parameter not found!");

                    return;
                }

                bool isOk;
                quint32 sequence = sequenceString.toUInt(&isOk);
                if (!isOk) {
                    emit rtspError("RTSP CSeq parameter parsing error!");

                    return;
                }

                auto function = sequenceToCallback_.take(sequence);
                if (function == nullptr) {
                    emit rtspError("RTSP callback for sequence not registered!");

                    return;
                }

                if ((this->*function)(message) != 0) {
                    emit rtspError("RTSP response processing error!");

                    return;
                }
            }
        }
    }
}