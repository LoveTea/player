#ifndef PLAYER_RTCP_H
#define PLAYER_RTCP_H

#include <QtNetwork/QUdpSocket>


namespace player::rtsp {
    class RTCP : public QUdpSocket {
        Q_OBJECT
        Q_DISABLE_COPY(RTCP)

    public:
        explicit RTCP(QObject* parent = nullptr);
        ~RTCP() override = default;

    private slots:
        void onReadyRead();
    };
}


#endif //PLAYER_RTCP_H
