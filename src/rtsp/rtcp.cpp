#include "rtcp.h"

namespace player::rtsp {
    RTCP::RTCP(QObject* parent) : QUdpSocket(parent) {
        connect(this, &RTCP::readyRead, this, &RTCP::onReadyRead);
    }

    void RTCP::onReadyRead() {
        while (hasPendingDatagrams()) {
            QByteArray datagram((int) pendingDatagramSize(), 0);
            readDatagram(datagram.data(), datagram.size(), nullptr, nullptr);
        }
    }
}