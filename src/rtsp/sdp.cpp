#include "sdp.h"
#include "rtsp.h"


namespace player::rtsp {
    // -------------------------- Media Class -----------------------------
    int SDP::Media::parseHeader(const QByteArray& data) {
        QList parts = QByteArray(data.data() + 2, data.size() - 2).split(' ');
        if (parts.size() < 4) {
            qCritical() << "SDP invalid media format!" << parts;

            return -1;
        }

        QByteArray mediaType = parts[0].toLower();
        if (mediaType == "audio") {
            type_ = Media::Type::AUDIO;
        } else if (mediaType == "video") {
            type_ = Media::Type::VIDEO;
        } else {
            qCritical() << "Mime type not supported!" << mediaType;

            return -1;
        }

        bool isOk;
        int position = parts[1].indexOf('/');
        port_ = (quint16) QByteArray(parts[1].data(), position).toInt(&isOk);
        if (!isOk) {
            qCritical() << "SDP media port number parse error!" << parts[1];

            return -1;
        }

        if (position == -1) {
            numberOfPorts_ = 1;
        } else {
            numberOfPorts_ = (quint16) QByteArray(parts[1].data(), position + 1).toInt(&isOk);
            if (!isOk) {
                qCritical() << "SDP media port count parse error!" << parts[1];

                return -1;
            }
        }

        proto_ = parts[2];
        formatId_ = parts[3].toInt(&isOk);
        if (!isOk) {
            qCritical() << "SDP media format parse error" << parts[3];

            return -1;
        }

        return 0;
    }

    int SDP::Media::parseAttribute(const QByteArray& data) {
        QByteArray attribute = QByteArray(data.data() + 2, data.size() - 2).trimmed();
        int position = attribute.indexOf(':');
        if (position == -1) {
            qCritical() << "SDP attribute parse error!" << attribute;

            return -1;
        }

        QByteArray key = QByteArray(attribute.data(), position).trimmed().toLower();
        QByteArray value = QByteArray(attribute.data() + position + 1, attribute.size() - (position + 1)).trimmed();

        if (key == "rtpmap") {
            if (int rc = parseRtpMapAttribute(value); rc != 0) {
                return rc;
            }
        }

        attributes_[key] = value;

        return 0;
    }

    int SDP::Media::parseRtpMapAttribute(const QByteArray& data) {
        QList parts = data.split(' ');
        if (parts.size() < 2) {
            qCritical() << "SDP media rtpmap attribute parsing error!" << data;

            return -1;
        }

        bool isOk;
        int formatId = parts[0].toInt(&isOk);
        if (!isOk) {
            qCritical() << "SDP media rtpmap format ID parsing error!" << data;

            return -1;
        }

        if (formatId == formatId_) {
            int position = parts[1].indexOf('/');
            if (position == -1) {
                qCritical() << "SDP media rtpmap format parsing error!" << data;

                return -1;
            }

            format_ = QByteArray(parts[1].data(), position);

            QByteArray temporary = QByteArray(parts[1].data() + position + 1, parts[1].size() - (position + 1)).trimmed();
            position = temporary.indexOf('/');
            clockRate_ = QByteArray(temporary.data(), position).toUInt(&isOk);
            if (!isOk) {
                qCritical() << "SDP media rtpmap clock rate parsing error!" << data;

                return -1;
            }

            if (position == -1) {
                channelsCount_ = 1;
            } else {
                channelsCount_ = (quint16) QByteArray(temporary.data() + position + 1,
                    temporary.size() - (position + 1)).toInt(&isOk);
                if (!isOk) {
                    qCritical() << "SDP media rtpmap channels count parsing error!" << data;

                    return -1;
                }
            }
        }

        return 0;
    }

    int SDP::Media::parseMPAProps(const QByteArray& data, SDP::Media::Properties& attributes) const {
        attributes.type = RTP::PayloadType::MPA;
        attributes.clockRate = clockRate_;

        return 0;
    }

    int SDP::Media::parseH264Props(const QByteArray& data, Properties& attributes) const {
        attributes.type = RTP::PayloadType::H264;
        attributes.clockRate = clockRate_;

        QList parts = data.split(';');
        for (const auto& prop : parts) {
            int position = prop.indexOf('=');
            if (position == -1) {
                continue;
            }

            bool isOk;
            QByteArray name = QByteArray(prop.data(), position).toLower().trimmed();
            QByteArray value = QByteArray(prop.data() + position + 1, prop.size() - (position + 1)).trimmed();
            if (name == "packetization-mode") {
                attributes.version = (quint8) value.toUInt(&isOk);
                if (!isOk) {
                    qWarning() << "SDP parsing H264 \"packetization-mode\" failed!";

                    continue;
                }
            } else if (name == "profile-level-id") {
                quint32 profileLevelId = value.toUInt(&isOk, 16);
                if (!isOk) {
                    qWarning() << "SDP parsing H264 \"profile-level-id\" failed!";

                    continue;
                }
                attributes.profile = quint8(profileLevelId >> 16);
                attributes.compatibility = quint8(profileLevelId >> 8);
                attributes.level = quint8(profileLevelId);
            } else if (name == "sprop-parameter-sets") {
                QList sprop = value.split(',');
                if (sprop.size() != 2) {
                    qWarning() << "SDP parsing H264 \"sprop-parameter-sets\" failed!";

                    continue;
                }
                attributes.sps = QByteArray::fromBase64(sprop[0]);
                attributes.pps = QByteArray::fromBase64(sprop[1]);
            }
        }

        return 0;
    }

    QByteArray SDP::Media::getAttribute(const QByteArray& key) const {
        auto iterator = attributes_.find(key.toLower());
        if (iterator == attributes_.end()) {
            return QByteArray();
        }

        return iterator.value();
    }

    SDP::Media::Properties SDP::Media::getMPAAttributes() const {
        Properties attributes = {};

        if (format_.toLower() == "mpa") {
            parseMPAProps(QByteArray(), attributes);
        }

        return attributes;
    }

    SDP::Media::Properties SDP::Media::getH264Attributes() const {
        Properties attributes = {};

        if (format_.toLower() == "h264") {
            QByteArray attribute = getAttribute("fmtp");
            QByteArray formatAsString = QByteArray::number(formatId_);
            if (attribute.startsWith(formatAsString)) {
                attribute = QByteArray(attribute.data() + formatAsString.size(),
                    attribute.size() - formatAsString.size()).trimmed();

                parseH264Props(attribute, attributes);
            }

            attribute = getAttribute("framesize");
            if (attribute.startsWith(formatAsString)) {
                attribute = QByteArray(attribute.data() + formatAsString.size(),
                    attribute.size() - formatAsString.size()).trimmed();

                QList parts = attribute.split('-');
                if (parts.size() == 2) {
                    attributes.width = parts[0].toInt();
                    attributes.height = parts[1].toInt();
                }
            }
        }

        return attributes;
    }

    // -------------------------- SDP Class -----------------------------
    SDP::SDP() : version_(0), level_(Level::SESSION) {}

    int SDP::parseVersion(const QByteArray& data) {
        bool isOk;
        version_ = QByteArray(data.data() + 2, data.size() - 2).toInt(&isOk);
        if (!isOk) {
            qCritical() << "SDP version parse error!" << data;

            return -1;
        }

        if (version_ != 0) {
            qCritical() << "SDP invalid version!" << version_;

            return -1;
        }

        return 0;
    }

    int SDP::parseAttribute(const QByteArray& data) {
        switch (level_) {
            case Level::SESSION:
                return parseSessionAttribute(data);

            case Level::MEDIA:
                return medias.back().parseAttribute(data);

            default:
                return 0;
        }
    }

    int SDP::parseSessionAttribute(const QByteArray& data) {
        sessionAttributes_.push_back(QByteArray(data.data() + 2, data.size() - 2).trimmed());

        return 0;
    }

    int SDP::deserialize(const QByteArray& data) {
        int processed = 0;

        while (true) {
            int position = data.indexOf(NEW_LINE, processed);
            if (position == -1) {
                break;
            }

            QByteArray line(data.data() + processed, position - processed);
            if (line.isEmpty()) {
                break;
            }

            if (line[1] != '=') {
                qCritical() << "SDP invalid format!" << line;

                return -1;
            }

            switch (line[0]) {
                case 'v':
                    if (int rc = parseVersion(line); rc != 0) {
                        return rc;
                    }

                    break;

                case 'o':
                    sessionOwner_ = QByteArray(line.data() + 2, line.size() - 2).trimmed();
                    break;

                case 's':
                    sessionName_ = QByteArray(line.data() + 2, line.size() - 2).trimmed();
                    break;

                case 'a':
                    if (int rc = parseAttribute(line); rc != 0) {
                        return rc;
                    }

                    break;

                case 'm': {
                    level_ = Level::MEDIA;
                    Media media;
                    if (media.parseHeader(line) != 0) {
                        level_ = Level::NOT_SUPPORTED;
                    } else {
                        medias.push_back(media);
                    }

                    break;
                }

                default:
                    qCritical() << "SDP parameter no supported!" << line;
                    break;
            }

            processed += line.size() + NEW_LINE.size();
        }

        return processed;
    }
}