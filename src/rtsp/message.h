#ifndef PLAYER_MESSAGE_H
#define PLAYER_MESSAGE_H

#include <QtCore/QUrl>
#include <QtCore/QHash>


namespace player::rtsp {
    class Message {
    public:
        enum class Type { REQUEST, RESPONSE };

        struct RangeHeader {
            QString type;
            float currentSec;
            float lengthSec;
        };

    private:
        int code_;
        QUrl url_;
        Type type_;
        QByteArray body_;
        QByteArray version_;
        QByteArray command_;
        QByteArray codeDescription_;
        QHash<QByteArray, QByteArray> headers_;

        int parseStartString(const QByteArray& data);
        int parseRequestStartString(const QByteArray& data);
        int parseResponseStartString(const QByteArray& data);

    public:
        explicit Message(Type type = Type::RESPONSE);
        virtual ~Message() = default;

        QByteArray serialize();
        int deserialize(const QByteArray& data);

        inline int getCode() const { return code_; }
        inline Type getType() const { return type_; }
        inline const QUrl& getUrl() const { return url_; }
        QByteArray getHeader(const QByteArray& key) const;
        inline const QByteArray& getBody() const { return body_; }
        inline const QByteArray& getCommand() const { return command_; }
        inline const QByteArray& getVersion() const { return version_; }
        inline const QByteArray& getCodeDescription() const { return codeDescription_; }

        inline void setCode(int code) { code_ = code; }
        inline void setType(Type type) { type_ = type; }
        inline void setUrl(const QUrl& url) { url_ = url; }
        inline void setBody(const QByteArray& body) { body_ = body; }
        inline void setCommand(const QByteArray& command) { command_ = command; }
        inline void setVersion(const QByteArray& version) { version_ = version; }
        inline void setHeader(const QByteArray& key, const QByteArray& value) { headers_[key] = value; }
        inline void setCodeDescription(const QByteArray& description) { codeDescription_ = description; }

        QList<QByteArray> parsePublicHeader() const;
        QHash<QByteArray, QByteArray> parseTransportHeader() const;
        RangeHeader parseRangeHeader() const;
    };
}


#endif //PLAYER_MESSAGE_H
