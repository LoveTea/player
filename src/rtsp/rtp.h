#ifndef PLAYER_RTP_H
#define PLAYER_RTP_H

#include <QtNetwork/QUdpSocket>


namespace player::rtsp {
    class RTP : public QUdpSocket {
        Q_OBJECT
        Q_DISABLE_COPY(RTP)

    public:
        enum class PayloadType { MPA, H264 };

        struct Header {
            quint8 version;
            quint8 padding;
            quint8 extension;
            quint8 csrcCount;
            quint8 marker;
            quint8 payloadType;
            quint16 sequenceNumber;
            quint32 timestamp;
            quint32 ssrc;

            int parseHeader(const QByteArray& data);
        };

        struct EncodedFrame {
            QByteArray data;
            PayloadType type;
            quint32 timestamp;
            quint16 sequenceNumber;
        };

    private:
        Header header_;
        PayloadType payloadType_;
        QByteArray fragmentationUnit_;

        int processMp3Nal(const QByteArray& data);
        int processH264Nal(const QByteArray& data);

    public:
        explicit RTP(PayloadType type, QObject* parent = nullptr);

        inline PayloadType getPayloadType() const { return payloadType_; }

    private slots:
        void onReadyRead();

    signals:
        void rtpError(QString what);
        void newFrame(EncodedFrame frame);
    };
}

Q_DECLARE_METATYPE(player::rtsp::RTP::EncodedFrame);


#endif //PLAYER_RTP_H
