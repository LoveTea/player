#include <QtCore/QDebug>

#include "rtsp.h"
#include "message.h"


namespace player::rtsp {
    Message::Message(player::rtsp::Message::Type type) : type_(type), code_(0), version_(VERSION) {}

    int Message::parseStartString(const QByteArray& data) {
        const int startLineSize = 10;
        if (data.size() < startLineSize) {
            return -1;
        }

        QByteArray startLine = QByteArray(data.data(), startLineSize).toLower();
        if (startLine.startsWith("rtsp")) {
            type_ = Type::RESPONSE;
            return parseResponseStartString(data);
        } else {
            type_ = Type::REQUEST;
            return parseRequestStartString(data);
        }
    }

    int Message::parseRequestStartString(const QByteArray& data) {
        qCritical() << __func__ << "Not implemented!";

        return -1;
    }

    int Message::parseResponseStartString(const QByteArray& data) {
        int currentPosition = data.indexOf(NEW_LINE);
        if (currentPosition == -1) {
            return currentPosition;
        }

        QByteArray startString = QByteArray(data.data(), currentPosition);
        QList parts = startString.split(' ');
        if (parts.size() != 3) {
            qCritical() << "RTSP parse start string failed!" << startString << parts;

            return -1;
        }

        version_ = parts[0];
        bool isOk = false;
        code_ = parts[1].toInt(&isOk);
        if (!isOk) {
            qCritical() << "RTSP parse error! Can't parse reply code!" << parts[1];

            return -1;
        }
        codeDescription_ = parts[2];

        return currentPosition + NEW_LINE.size();
    }

    QByteArray Message::serialize() {
        QByteArray data;

        data.push_back(command_);
        data.push_back(' ');
        data.push_back(url_.toEncoded());
        data.push_back(' ');
        data.push_back(version_);
        data.push_back(NEW_LINE);

        auto iterator = headers_.begin();
        while (iterator != headers_.end()) {
            data.push_back(iterator.key());
            data.push_back(": ");
            data.push_back(iterator.value());
            data.push_back(NEW_LINE);

            ++iterator;
        }
        data.push_back(NEW_LINE);

        if (!body_.isEmpty()) {
            data.push_back(body_);
        }

        return data;
    }

    int Message::deserialize(const QByteArray& data) {
        QByteArray line;
        int processed = 0;
        int currentPosition = parseStartString(data);
        if (currentPosition == -1) {
            return -1;
        }

        processed += currentPosition;

        while (true) {
            int lineStart = currentPosition;
            currentPosition = data.indexOf(NEW_LINE, lineStart);
            if (currentPosition == -1) {
                break;
            }

            line = QByteArray(data.data() + lineStart, currentPosition - lineStart);
            processed += line.size() + NEW_LINE.size();
            currentPosition += NEW_LINE.size();
            if (line.isEmpty()) {
                QByteArray contentLengthString = getHeader("Content-Length");
                if (!contentLengthString.isEmpty()) {
                    bool isOk;
                    int contentLength = contentLengthString.toInt(&isOk);
                    if (!isOk) {
                        qCritical() << "RTSP not valid content length!" << contentLengthString;

                        break;
                    }

                    if (contentLength > data.size() - processed) {
                        return 0;
                    }

                    body_ = QByteArray(data.data() + currentPosition, contentLength);
                    processed += body_.size();
                }

                break;
            }

            int pos = line.indexOf(':');
            if (pos == -1) {
                qCritical() << "RTSP not valid header!" << line;
            } else {
                QByteArray key = QByteArray(line.data(), pos).trimmed().toLower();
                QByteArray value = QByteArray(line.data() + pos + 1, line.size() - (pos + 1)).trimmed();
                headers_[key] = value;
            }
        }

        return processed;
    }

    QByteArray Message::getHeader(const QByteArray& key) const {
        auto iterator = headers_.find(key.toLower());
        if (iterator == headers_.end()) {
            return QByteArray();
        }

        return iterator.value();
    }

    QList<QByteArray> Message::parsePublicHeader() const {
        QList<QByteArray> allowedCommands;

        QByteArray header = getHeader("Public");
        if (!header.isEmpty()) {
            header = header.toUpper().replace(' ', "");
            allowedCommands = header.split(',');
        }

        return allowedCommands;
    }

    QHash<QByteArray, QByteArray> Message::parseTransportHeader() const {
        QHash<QByteArray, QByteArray> parameters;

        QByteArray header = getHeader("Transport");
        if (!header.isEmpty()) {
            QList parts = header.toLower().split(';');
            for (const auto& part : parts) {
                QList keyValue = part.toLower().split('=');
                if (keyValue.size() == 1) {
                    parameters[keyValue[0].trimmed()] = QByteArray();
                } else {
                    parameters[keyValue[0].trimmed()] = keyValue[1].trimmed();
                }
            }
        }

        return parameters;
    }

    Message::RangeHeader Message::parseRangeHeader() const {
        RangeHeader range = {};

        QByteArray header = getHeader("Range");
        if (!header.isEmpty()) {
            QList parts = header.split('=');
            if (parts.size() == 2) {
                range.type = parts[0];
                parts = parts[1].split('-');
                if (parts.size() == 2) {
                    range.currentSec = parts[0].toFloat();
                    range.lengthSec = parts[1].toFloat();
                }
            }
        }

        return range;
    }
}