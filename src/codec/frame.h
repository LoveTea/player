#ifndef PLAYER_FRAME_H
#define PLAYER_FRAME_H

#include <QtCore/QByteArray>
#include <QtMultimedia/QVideoFrame>


namespace player::codec {
    struct EncodedAudioFrame {
        quint32 timestamp;
        QByteArray data;
    };

    struct DecodedAudioFrame {
        quint32 timestamp;
        QByteArray data;
    };

    struct EncodedVideoFrame {
        quint32 timestamp;
        QByteArray data;
    };

    struct DecodedVideoFrame {
        quint32 timestamp;
        QVideoFrame data;
    };
}

Q_DECLARE_METATYPE(player::codec::EncodedAudioFrame);
Q_DECLARE_METATYPE(player::codec::DecodedAudioFrame);
Q_DECLARE_METATYPE(player::codec::EncodedVideoFrame);
Q_DECLARE_METATYPE(player::codec::DecodedVideoFrame);

#endif //PLAYER_FRAME_H
