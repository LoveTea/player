#ifndef PLAYER_MP3_H
#define PLAYER_MP3_H

extern "C" {
#include <libavformat/avformat.h>
#include <libswresample/swresample.h>
};

#include "../codec.h"
#include "../frame.h"


namespace player::codec::audio {
    class MP3 : public Codec {
        Q_OBJECT

    private:
        AVCodec* codec_;
        SwrContext* swrContext_;
        AVCodecContext* context_;

        int openCodecContext();
        int openSwrContext(const AVFrame* sourceFrame, const AVFrame* destinationFrame);
        int putEncodedFrame(codec::EncodedAudioFrame& frame);
        int getDecodedFrames();

    public:
        explicit MP3(QObject* parent = nullptr);
        ~MP3() override;

    public:
        void onDecoderSettings(QVariant settings) override;
        void onEncodedFrame(QVariant frame) override;
    };
}


#endif //PLAYER_MP3_H
