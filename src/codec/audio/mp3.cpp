#include <QtCore/QDebug>
#include <QtCore/QVariant>
#include <QtCore/QSharedPointer>

#include "mp3.h"
#include "../frame.h"

namespace player::codec::audio {
    MP3::MP3(QObject* parent) : Codec(Codec::Format::MP3, parent), codec_(nullptr), context_(nullptr),
        swrContext_(nullptr) {}

    MP3::~MP3() {
        if (context_) {
            avcodec_free_context(&context_);
        }

        if (swrContext_) {
            swr_free(&swrContext_);
        }
    }

    int MP3::openCodecContext() {
        codec_ = avcodec_find_decoder(AV_CODEC_ID_MP3);
        if (!codec_) {
            emit codecError("MP3 decoder not found!");

            return -1;
        }

        context_ = avcodec_alloc_context3(codec_);
        if (!context_) {
            emit codecError("MP3 context allocation failed!");

            return -1;
        }

        if (avcodec_open2(context_, codec_, nullptr) < 0) {
            emit codecError("H264 decoder not found!");

            return -1;
        }

        return 0;
    }

    int MP3::openSwrContext(const AVFrame* sourceFrame, const AVFrame* destinationFrame) {
        swrContext_ = swr_alloc_set_opts(nullptr, destinationFrame->channel_layout, (AVSampleFormat) destinationFrame->format,
            destinationFrame->sample_rate, sourceFrame->channel_layout, (AVSampleFormat) sourceFrame->format,
            sourceFrame->sample_rate, 0, nullptr);
        if (!swrContext_) {
            emit codecError("MP3 failed to allocate swr context!");

            return -1;
        }

        return 0;
    }

    int MP3::putEncodedFrame(codec::EncodedAudioFrame& frame) {
        AVPacket packet;
        av_init_packet(&packet);
        packet.data = (quint8*) frame.data.data();
        packet.size = frame.data.size();
        packet.pts = frame.timestamp;

        if (int rc = avcodec_send_packet(context_, &packet); rc == 0) {
            return 0;
        } else if (rc == AVERROR(EAGAIN)) {
            return 0;
        } else {
            return -1;
        }
    }

    int MP3::getDecodedFrames() {
        while (true) {
            QSharedPointer<AVFrame> originFrame(av_frame_alloc(), [](AVFrame* frame) { av_frame_free(&frame); });
            QSharedPointer<AVFrame> s16Frame(av_frame_alloc(),
                [](AVFrame* frame) { av_freep(frame->data); av_frame_free(&frame); });
            if (int rc = avcodec_receive_frame(context_, originFrame.data()); rc == 0) {
                s16Frame->format = AV_SAMPLE_FMT_S16;
                s16Frame->channels = 2;
                s16Frame->pts = originFrame->pts;
                s16Frame->nb_samples = originFrame->nb_samples;
                s16Frame->sample_rate = 44100;
                s16Frame->channel_layout = AV_CH_LAYOUT_STEREO;

                if (!swrContext_) {
                    openSwrContext(originFrame.data(), s16Frame.data());
                }

                rc = av_samples_alloc(s16Frame->data, s16Frame->linesize, s16Frame->channels, s16Frame->nb_samples,
                    (AVSampleFormat) s16Frame->format, 0);
                if (rc < 0) {
                    emit codecError("MP3 frame allocation failed!");

                    return -1;
                }

                rc = swr_convert_frame(swrContext_, s16Frame.data(), originFrame.data());
                if (rc != 0) {
                    emit codecError("MP3 frame conversion failed!");

                    return -1;
                }

                codec::DecodedAudioFrame audioFrame = {
                    .timestamp = (quint32) s16Frame->pts,
                    .data = QByteArray((const char*) s16Frame->data[0], s16Frame->linesize[0])
                };
                emit decodedFrame(QVariant::fromValue(audioFrame));
            } else if (rc == AVERROR(EAGAIN)) {
                return 0;
            } else {
                emit codecError("H264 decoder get frame failed!");

                return -1;
            }
        }
    }

    void MP3::onDecoderSettings(QVariant settings) {

    }

    void MP3::onEncodedFrame(QVariant frame) {
        if (!context_) {
            openCodecContext();
        }

        auto audioFrame = frame.value<codec::EncodedAudioFrame>();

        putEncodedFrame(audioFrame);

        getDecodedFrames();
    }
}