#ifndef PLAYER_YUV420P_BUFFER_H
#define PLAYER_YUV420P_BUFFER_H

#include <QtCore/QList>
#include <QtMultimedia/QAbstractPlanarVideoBuffer>


namespace player::codec::video {
    class YUV420PBuffer : public QAbstractVideoBuffer {
    public:
        static const int LINE_COUNT = 3;

    private:
        MapMode mapMode_;
        QByteArray data_;
        QList<int> lineSize_;

    public:
        YUV420PBuffer(quint8** data, int* lineSize, int height);

        uchar* map(MapMode mode, int* numBytes, int* bytesPerLine) override;
        MapMode mapMode() const override;
        void release() override;
        void unmap() override;
    };
}


#endif //PLAYER_YUV420P_BUFFER_H
