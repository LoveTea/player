#ifndef PLAYER_H264_H
#define PLAYER_H264_H

extern "C" {
#include <libswscale/swscale.h>
#include <libavformat/avformat.h>
};

#include "../codec.h"
#include "../frame.h"


namespace player::codec::video {
    class H264 : public Codec {
        Q_OBJECT

    public:
        struct Params {
            quint8 version;
            quint8 profile;
            quint8 compatibility;
            quint8 level;
            QByteArray sps;
            QByteArray pps;
            int width;
            int height;
        };

    private:
        Params params_;
        AVCodec* codec_;
        SwsContext* swsContext_;
        AVCodecContext* context_;

        int openCodecContext();
        int openSwsContext(const AVFrame* sourceFrame, const AVFrame* destinationFrame);
        int getDecodedFrames();
        int putEncodedFrame(codec::EncodedVideoFrame& frame);

    public:
        explicit H264(QObject* parent = nullptr);
        ~H264() override;

    public slots:
        void onDecoderSettings(QVariant settings) override;
        void onEncodedFrame(QVariant frame) override;
    };
}

Q_DECLARE_METATYPE(player::codec::video::H264::Params);


#endif //PLAYER_H264_H
