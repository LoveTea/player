#include <QtCore/QDebug>

#include "yuv420p_buffer.h"


namespace player::codec::video {
    YUV420PBuffer::YUV420PBuffer(quint8** data, int* lineSize, int height)
        : QAbstractVideoBuffer(QAbstractVideoBuffer::NoHandle), mapMode_(NotMapped)
    {
        for (int i = 0; i < LINE_COUNT; ++i) {
            lineSize_.push_back(lineSize[i]);
            data_.append((const char*) data[i], lineSize_[i] * height / (i == 0 ? 1 : 2));
        }
    }

    uchar* YUV420PBuffer::map(QAbstractVideoBuffer::MapMode mode, int* numBytes, int* bytesPerLine) {
        mapMode_ = mode;
        *bytesPerLine = lineSize_[0];
        *numBytes += data_.size();

        return (quint8*) data_.data();
    }

    QAbstractVideoBuffer::MapMode YUV420PBuffer::mapMode() const {
        return mapMode_;
    }

    void YUV420PBuffer::release() {
        delete this;
    }

    void YUV420PBuffer::unmap() {
        mapMode_ = NotMapped;
    }
}