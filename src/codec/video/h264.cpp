#include <QtCore/QDebug>
#include <QtCore/QtEndian>
#include <QtCore/QVariant>
#include <QtCore/QSharedPointer>
#include <QtMultimedia/QVideoFrame>

extern "C" {
#include <libavutil/imgutils.h>
}

#include "h264.h"
#include "../frame.h"
#include "yuv420p_buffer.h"

namespace player::codec::video {
    H264::H264(QObject* parent) : Codec(Codec::Format::H264, parent), params_(), codec_(nullptr), context_(nullptr),
        swsContext_(nullptr) {}

    H264::~H264() {
        if (context_) {
            avcodec_free_context(&context_);
        }

        if (swsContext_) {
            sws_freeContext(swsContext_);
        }
    }

    int H264::openCodecContext() {
        codec_ = avcodec_find_decoder(AV_CODEC_ID_H264);
        if (!codec_) {
            emit codecError("H264 decoder not found!");

            return -1;
        }

        context_ = avcodec_alloc_context3(codec_);
        if (!context_) {
            emit codecError("H264 context allocation failed!");

            return -1;
        }

        quint16 spsSize = (quint16) params_.sps.size();
        quint16 ppsSize = (quint16) params_.pps.size();
        if (spsSize == 0 || ppsSize == 0) {
            emit codecError("H264 SPS or PPS parameters not presented!");

            return -1;
        }

        if (params_.width == 0 || params_.height == 0) {
            emit codecError("H264 picture width or height not presented!");

            return -1;
        }

        int extraDataLength = 0;
        quint8* extraData = new quint8[BUFSIZ];
        extraData[extraDataLength] = params_.version; ++extraDataLength;
        extraData[extraDataLength] = params_.profile; ++extraDataLength;
        extraData[extraDataLength] = params_.compatibility; ++extraDataLength;
        extraData[extraDataLength] = params_.level; ++extraDataLength;
        extraData[extraDataLength] = quint8(0xfc | 3); ++extraDataLength; // length minus one
        extraData[extraDataLength] = quint8(0xe0 | 1); ++extraDataLength; // sps parameters count
        extraData[extraDataLength] = quint8(spsSize >> 8); ++extraDataLength;
        extraData[extraDataLength] = quint8(spsSize); ++extraDataLength;
        memcpy(extraData + extraDataLength, params_.sps.data(), spsSize); extraDataLength += spsSize;
        extraData[extraDataLength] = 1; ++extraDataLength; // pps parameters count
        extraData[extraDataLength] = quint8(ppsSize >> 8); ++extraDataLength;
        extraData[extraDataLength] = quint8(ppsSize); ++extraDataLength;
        memcpy(extraData + extraDataLength, params_.pps.data(), ppsSize); extraDataLength += ppsSize;

        context_->extradata = extraData;
        context_->extradata_size = extraDataLength;
        context_->width = params_.width;
        context_->height = params_.height;

        if (avcodec_open2(context_, codec_, nullptr) < 0) {
            emit codecError("H264 decoder not found!");

            return -1;
        }

        return 0;
    }

    int H264::openSwsContext(const AVFrame* sourceFrame, const AVFrame* destinationFrame) {
        swsContext_ = sws_getContext(sourceFrame->width, sourceFrame->height, (AVPixelFormat) sourceFrame->format,
            destinationFrame->width, destinationFrame->height, (AVPixelFormat) destinationFrame->format, SWS_BICUBIC,
            nullptr, nullptr, nullptr);
        if (swsContext_ == nullptr) {
            emit decodedFrame("H264 scale context allocation error!");

            return -1;
        }

        return 0;
    }

    int H264::getDecodedFrames() {
        while (true) {
            AVFrame* frame;
            QSharedPointer<AVFrame> yuvFrame;
            QSharedPointer<AVFrame> originFrame(av_frame_alloc(), [](AVFrame* frame) { av_frame_free(&frame); });
            if (int rc = avcodec_receive_frame(context_, originFrame.data()); rc == 0) {
                if (originFrame->format == AV_PIX_FMT_YUV420P) {
                    frame = originFrame.data();
                } else {
                    yuvFrame.reset(av_frame_alloc(), [](AVFrame* frame) { av_freep(frame->data); av_frame_free(&frame); });
                    yuvFrame->width = originFrame->width;
                    yuvFrame->height = originFrame->height;
                    yuvFrame->format = AV_PIX_FMT_YUV420P;
                    yuvFrame->pts = originFrame->pts;

                    if (!swsContext_) {
                        openSwsContext(originFrame.data(), yuvFrame.data());
                    }

                    rc = av_image_alloc(yuvFrame->data, yuvFrame->linesize, yuvFrame->width, yuvFrame->height,
                        (AVPixelFormat) yuvFrame->format, 32);
                    if (rc < 0) {
                        qCritical() << "H264 frame allocation failed!";

                        return -1;
                    }

                    sws_scale(swsContext_, originFrame->data, originFrame->linesize, 0, yuvFrame->height, yuvFrame->data,
                        yuvFrame->linesize);

                    frame = yuvFrame.data();
                }

                YUV420PBuffer* frameBuffer = new YUV420PBuffer(frame->data, frame->linesize, frame->height);
                codec::DecodedVideoFrame videoFrame = {
                    .timestamp = (quint32) frame->pts,
                    .data = QVideoFrame(frameBuffer, { frame->width, frame->height }, QVideoFrame::Format_YUV420P)
                };
                emit decodedFrame(QVariant::fromValue(videoFrame));
            } else if (rc == AVERROR(EAGAIN)) {
                return 0;
            } else {
                emit codecError("H264 decoder get frame failed!");

                return -1;
            }
        }
    }

    int H264::putEncodedFrame(codec::EncodedVideoFrame& frame) {
        quint32 frameSize = (quint32) qToBigEndian(frame.data.size());
        QByteArray frameSizeHeader(4, 0);
        memcpy(frameSizeHeader.data(), &frameSize, sizeof(frameSize));
        frame.data.prepend(frameSizeHeader);

        AVPacket packet;
        av_init_packet(&packet);
        packet.data = (quint8*) frame.data.data();
        packet.size = frame.data.size();
        packet.pts = frame.timestamp;

#ifdef DEBUG
        qDebug() << "ENCODED FRAME" << packet.size << frame.data.size();
        for (int i = 0; i < frame.data.size(); ++i) {
            if (i != 0 && i % 75 == 0) {
                printf("\n");
            }
            printf("%02x ", quint8(frame.data.data()[i]));
        }
#endif

        if (int rc = avcodec_send_packet(context_, &packet); rc == 0) {
            return 0;
        } else if (rc == AVERROR(EAGAIN)) {
            return 0;
        } else {
            return -1;
        }
    }

    void H264::onDecoderSettings(QVariant settings) {
        if (context_) {
            avcodec_free_context(&context_);
        }

        params_ = settings.value<Params>();

        openCodecContext();
    }

    void H264::onEncodedFrame(QVariant frame) {
        if (!context_) {
            emit codecError("H264 decoder context not configured!");

            return;
        }

        auto videoFrame = frame.value<codec::EncodedVideoFrame>();

        putEncodedFrame(videoFrame);

        getDecodedFrames();
    }
}