#ifndef PLAYER_CODEC_H
#define PLAYER_CODEC_H

#include <QtCore/QObject>


namespace player::codec {
    class Codec : public QObject {
        Q_OBJECT

    public:
        enum class Format { MP3, H264 };

    private:
        Format format_;

    public:
        explicit Codec(Format format, QObject* parent = nullptr);

    public slots:
        virtual void onDecoderSettings(QVariant settings) = 0;
        virtual void onEncodedFrame(QVariant frame) = 0;

    signals:
        void codecError(QString what);
        void decodedFrame(QVariant frame);
    };
}


#endif //PLAYER_CODEC_H
